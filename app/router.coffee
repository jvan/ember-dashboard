`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
   location: config.locationType

Router.map ->

   #----- Dashboard Routes --------------------------------------

   # Provides an overview of the system.
   @route 'dashboard'


   #----- User Routes -------------------------------------------

   # Displays all users ond the system with sub-routes
   # for filtering users based on user type.
   @route 'users', ->
      @route 'all'     # show all users on the system
      @route 'admin'   # show only admin users
      @route 'managed' # show only managed users

   # Displays a form for creating a new user on the system.
   @route 'user.new', path: '/users/new'
   @route 'user.new.bulk-upload', path: '/users/new/bulk-upload'

   # Displays details about a particular user.
   @resource 'user', path: '/users/:user_id', ->
      @route 'profile'  # show/edit user profile data
      @route 'settings' # show/edit user settings
      @route 'activity' # recent actions taken by user
      #@route 'edit', path: '/edit' # TODO: Delete user/edit route and associated resources.


   #----- Accounts Routes ---------------------------------------

   # Display all accounts on the system.
   @route 'accounts'

   # Display form for creating a new account.
   @route 'account.new', path: '/accounts/new'

   # Displays details about a particular account.
   @route 'account', path: '/accounts/:account_id', ->
      @route 'profile' # show/edit account profile data
      @route 'billing' # show/edit acconut billiing data
      @route 'users'   # display list of all users belonging to account


   #----- Log Routes --------------------------------------------

   # Display site activity.
   @route 'log', ->
      @route 'system'   # display system logs
      @route 'activity' # display user activity


   #----- Utilities Routes --------------------------------------

   # System tools and utilities.
   @route 'utilities', ->
      @route 'platform-alert' # broadcast a message to all site users

`export default Router`
