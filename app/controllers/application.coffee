`import Ember from 'ember'`

ApplicationController = Ember.Controller.extend
   #----- Dependencies ------------------------------------------
   needs: ['users', 'accounts']
   users:    Em.computed.alias 'controllers.users'
   accounts: Em.computed.alias 'controllers.accounts'

   #----- Controller Properties ---------------------------------
   platformAlert: null

   actions:
      broadcast: (message) ->
         console.log "[broadcast]: #{message}"
         @set 'platformAlert', message

`export default ApplicationController`
