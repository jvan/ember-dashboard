`import Ember from 'ember'`

IndexRoute = Ember.Route.extend
   # Automatically re-direct to dashboard route.
   beforeModel: ->
      @transitionTo 'dashboard'

`export default IndexRoute`
