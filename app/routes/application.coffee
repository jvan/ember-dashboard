`import Ember from 'ember'`

ApplicationRoute = Ember.Route.extend
   setupController: (controller, model) ->
      # Pre-load all user and account data.
      @store.find('user').then (users) =>
         @controllerFor('users').set 'allUsers', users

      @store.find('account').then (accounts) =>
         @controllerFor('accounts').set 'content', accounts

`export default ApplicationRoute`
