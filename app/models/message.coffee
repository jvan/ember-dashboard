`import DS from 'ember-data'`

Message = DS.Model.extend

   #----- Attributes --------------------------------------------

   text:     DS.attr 'string'
   date:     DS.attr 'date'
   logLevel: DS.attr 'string', defaultValue: ''

   #----- Relationships -----------------------------------------

   user: DS.belongsTo 'user', async: true

   #----- Computed Properties -----------------------------------

   alertClass: Em.computed 'logLevel', ->
     "alert-#{@get 'logLevel'}"

   alertIcon: Em.computed 'logLevel', ->
      if @get('logLevel') == 'info'
         'info-circle'
      else if @get('logLevel')== 'warning'
         'exclamation-circle'
      else
         'warning'

#----- Fixture Data ------------------------------------------

Message.reopenClass
   FIXTURES: [
      {"id":1,"text":"Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.","date":"2014-12-06T04:51:46Z","user":"13"},
      {"id":2,"text":"Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.","date":"2014-08-11T08:15:45Z","user":"7"},
      {"id":3,"text":"Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.","date":"2014-08-25T03:31:42Z","user":"8"},
      {"id":4,"text":"Sed sagittis.","date":"2014-07-14T18:02:02Z","user":"3"},
      {"id":5,"text":"Morbi quis tortor id nulla ultrices aliquet.","date":"2014-10-18T04:02:55Z","user":"2"},
      {"id":6,"text":"Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.","date":"2014-09-14T15:51:23Z","user":"4"},
      {"id":7,"text":"Etiam pretium iaculis justo.","date":"2014-08-04T02:48:51Z","user":"1"},
      {"id":8,"text":"Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.","date":"2014-04-14T02:49:14Z","user":"14"},
      {"id":9,"text":"Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla.","date":"2014-11-07T22:05:21Z","user":"13"},
      {"id":10,"text":"Etiam justo. Etiam pretium iaculis justo.","date":"2014-02-27T14:01:14Z","user":"9"},
      {"id":11,"text":"Quisque ut erat.","date":"2014-09-20T14:24:25Z","user":"10"},
      {"id":12,"text":"Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.","date":"2014-05-15T22:23:04Z","user":"1"},
      {"id":13,"text":"Ut at dolor quis odio consequat varius. Integer ac leo.","date":"2014-06-26T17:21:08Z","user":"18"},
      {"id":14,"text":"Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.","date":"2014-06-29T02:48:18Z","user":"15"},
      {"id":15,"text":"Cras in purus eu magna vulputate luctus.","date":"2014-09-24T11:40:12Z","user":"1"},
      {"id":16,"text":"Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.","date":"2014-08-12T17:31:40Z","user":"17"},
      {"id":17,"text":"Donec semper sapien a libero.","date":"2014-09-20T03:49:43Z","user":"18"},
      {"id":18,"text":"Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.","date":"2014-04-27T10:38:50Z","user":"6"},
      {"id":19,"text":"Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.","date":"2014-07-19T12:51:49Z","user":"20"},
      {"id":20,"text":"Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.","date":"2014-12-19T04:18:06Z","user":"19"},
      {"id":21,"text":"Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.","date":"2014-03-30T01:58:50Z","user":"6"},
      {"id":22,"text":"Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.","date":"2014-12-20T16:08:49Z","user":"9"},
      {"id":23,"text":"Suspendisse potenti.","date":"2014-04-27T16:39:47Z","user":"3"},
      {"id":24,"text":"Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.","date":"2015-02-04T06:37:29Z","user":"2"},
      {"id":25,"text":"Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.","date":"2014-04-24T20:54:17Z","user":"10"},
      {"id":26,"text":"Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.","date":"2014-12-27T19:58:43Z","user":"17"},
      {"id":27,"text":"Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.","date":"2014-12-24T10:58:07Z","user":"5"},
      {"id":28,"text":"Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.","date":"2014-07-29T18:38:45Z","user":"19"},
      {"id":29,"text":"Proin at turpis a pede posuere nonummy.","date":"2014-04-24T06:49:39Z","user":"18"},
      {"id":30,"text":"Phasellus id sapien in sapien iaculis congue.","date":"2015-01-29T15:18:25Z","user":"18"},
      {"id":31,"text":"Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.","date":"2014-06-03T07:31:58Z","user":"5"},
      {"id":32,"text":"Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.","date":"2014-07-27T00:14:39Z","user":"20"},
      {"id":33,"text":"Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.","date":"2014-11-20T13:27:33Z","user":"4"},
      {"id":34,"text":"Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.","date":"2014-09-25T18:06:02Z","user":"7"},
      {"id":35,"text":"Morbi non quam nec dui luctus rutrum.","date":"2014-03-21T23:16:10Z","user":"7"},
      {"id":36,"text":"Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.","date":"2014-08-21T12:43:44Z","user":"18"},
      {"id":37,"text":"Suspendisse potenti. In eleifend quam a odio.","date":"2015-01-05T18:13:00Z","user":"7"},
      {"id":38,"text":"Donec posuere metus vitae ipsum. Aliquam non mauris.","date":"2014-07-30T20:53:30Z","user":"10"},
      {"id":39,"text":"Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.","date":"2014-12-01T05:58:42Z","user":"10"},
      {"id":40,"text":"Aliquam sit amet diam in magna bibendum imperdiet.","date":"2015-01-06T15:10:53Z","user":"2"},
      {"id":41,"text":"Duis aliquam convallis nunc.","date":"2014-04-19T11:32:23Z","user":"15"},
      {"id":42,"text":"Mauris lacinia sapien quis libero.","date":"2014-02-18T01:02:36Z","user":"18"},
      {"id":43,"text":"In quis justo.","date":"2014-09-11T03:30:33Z","user":"15"},
      {"id":44,"text":"Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.","date":"2015-01-31T10:46:18Z","user":"18"},
      {"id":45,"text":"Suspendisse potenti.","date":"2014-05-01T22:02:57Z","user":"20"},
      {"id":46,"text":"Morbi a ipsum.","date":"2014-03-27T23:40:35Z","user":"20"},
      {"id":47,"text":"Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.","date":"2014-04-25T19:35:51Z","user":"5"},
      {"id":48,"text":"Aenean fermentum.","date":"2014-04-15T12:04:55Z","user":"14"},
      {"id":49,"text":"Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.","date":"2014-12-21T19:02:49Z","user":"4"},
      {"id":50,"text":"Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.","date":"2014-03-10T17:17:54Z","user":"10"},
      {"id":51,"text":"Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.","date":"2014-09-25T04:08:41Z","user":"15"},
      {"id":52,"text":"Quisque porta volutpat erat.","date":"2014-02-27T08:55:44Z","user":"17"},
      {"id":53,"text":"Sed ante.","date":"2015-01-30T21:43:19Z","user":"16"},
      {"id":54,"text":"Phasellus sit amet erat.","date":"2014-04-13T11:29:23Z","user":"19"},
      {"id":55,"text":"Nulla ac enim.","date":"2014-12-27T10:13:15Z","user":"10"},
      {"id":56,"text":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","date":"2015-01-23T17:22:14Z","user":"17"},
      {"id":57,"text":"Aliquam erat volutpat. In congue.","date":"2014-12-11T13:54:11Z","user":"19"},
      {"id":58,"text":"Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.","date":"2014-04-22T22:00:10Z","user":"8"},
      {"id":59,"text":"Aenean fermentum. Donec ut mauris eget massa tempor convallis.","date":"2014-05-04T18:37:53Z","user":"10"},
      {"id":60,"text":"Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.","date":"2014-08-13T02:24:40Z","user":"15"},
      {"id":61,"text":"Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.","date":"2014-11-01T05:50:15Z","user":"19"},
      {"id":62,"text":"Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.","date":"2014-06-03T23:55:02Z","user":"13"},
      {"id":63,"text":"Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.","date":"2014-05-15T15:09:16Z","user":"12"},
      {"id":64,"text":"Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.","date":"2014-06-03T16:57:08Z","user":"11"},
      {"id":65,"text":"Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.","date":"2014-03-29T12:40:27Z","user":"8"},
      {"id":66,"text":"Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.","date":"2014-09-24T23:06:03Z","user":"18"},
      {"id":67,"text":"Duis mattis egestas metus.","date":"2014-03-13T00:11:16Z","user":"5"},
      {"id":68,"text":"Suspendisse accumsan tortor quis turpis.","date":"2014-04-04T03:15:43Z","user":"14"},
      {"id":69,"text":"Quisque ut erat. Curabitur gravida nisi at nibh.","date":"2014-07-23T08:04:00Z","user":"5"},
      {"id":70,"text":"Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.","date":"2015-02-01T10:36:29Z","user":"10"},
      {"id":71,"text":"In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.","date":"2014-05-05T06:16:31Z","user":"20"},
      {"id":72,"text":"Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.","date":"2014-04-09T16:51:38Z","user":"1"},
      {"id":73,"text":"Nullam varius. Nulla facilisi.","date":"2014-04-17T13:08:55Z","user":"18"},
      {"id":74,"text":"Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.","date":"2014-08-20T19:00:10Z","user":"13"},
      {"id":75,"text":"Phasellus in felis. Donec semper sapien a libero. Nam dui.","date":"2014-09-07T02:36:26Z","user":"18"},
      {"id":76,"text":"Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.","date":"2014-06-09T00:02:51Z","user":"10"},
      {"id":77,"text":"Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.","date":"2015-01-01T06:54:12Z","user":"10"},
      {"id":78,"text":"Pellentesque viverra pede ac diam.","date":"2014-08-26T06:19:47Z","user":"13"},
      {"id":79,"text":"Vivamus vel nulla eget eros elementum pellentesque.","date":"2014-08-12T03:11:11Z","user":"2"},
      {"id":80,"text":"Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.","date":"2014-11-17T14:22:54Z","user":"11"},
      {"id":81,"text":"Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.","date":"2014-11-04T01:04:33Z","user":"4"},
      {"id":82,"text":"Nulla facilisi.","date":"2014-07-23T12:33:55Z","user":"9"},
      {"id":83,"text":"Praesent blandit. Nam nulla.","date":"2014-07-09T16:16:59Z","user":"6"},
      {"id":84,"text":"Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis.","date":"2014-06-22T11:40:21Z","user":"6"},
      {"id":85,"text":"Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.","date":"2014-11-30T13:39:19Z","user":"18"},
      {"id":86,"text":"Ut at dolor quis odio consequat varius.","date":"2014-08-17T20:58:36Z","user":"16"},
      {"id":87,"text":"Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.","date":"2014-12-25T09:31:34Z","user":"13"},
      {"id":88,"text":"Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.","date":"2014-06-10T06:55:53Z","user":"1"},
      {"id":89,"text":"Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.","date":"2014-07-25T13:39:54Z","user":"17"},
      {"id":90,"text":"Curabitur at ipsum ac tellus semper interdum.","date":"2014-12-11T22:00:35Z","user":"8"},
      {"id":91,"text":"Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.","date":"2014-11-28T13:09:44Z","user":"15"},
      {"id":92,"text":"Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.","date":"2014-04-21T07:57:12Z","user":"20"},
      {"id":93,"text":"Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.","date":"2014-04-20T06:04:14Z","user":"5"},
      {"id":94,"text":"Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.","date":"2014-10-22T03:34:51Z","user":"2"},
      {"id":95,"text":"Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.","date":"2014-03-10T15:10:03Z","user":"14"},
      {"id":96,"text":"Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.","date":"2014-07-23T14:56:46Z","user":"16"},
      {"id":97,"text":"Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.","date":"2014-05-08T05:15:52Z","user":"9"},
      {"id":98,"text":"Aliquam erat volutpat. In congue. Etiam justo.","date":"2014-04-06T07:49:47Z","user":"12"},
      {"id":99,"text":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.","date":"2014-08-22T19:35:04Z","user":"16"},
      {"id":100,"text":"Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.","date":"2014-07-29T14:24:26Z","user":"9"}
   ]


`export default Message`
