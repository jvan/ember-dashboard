`import DS from 'ember-data'`

User = DS.Model.extend

   #----- Attributes --------------------------------------------

   first:     DS.attr 'string'
   last:      DS.attr 'string'
   email:     DS.attr 'string'
   spend:     DS.attr 'number',  defaultValue: 0
   isAdmin:   DS.attr 'boolean', defaultValue: false
   isManaged: DS.attr 'boolean', defaultValue: false

   #----- Relationships -----------------------------------------

   account:  DS.belongsTo 'account', async: true
   activity: DS.hasMany 'message',   async: true

   #----- Computed Properties -----------------------------------

   fullname: Em.computed 'first', 'last', ->
      "#{@get 'first'} #{@get 'last'}"

   # Return url for identicon image.
   #
   # An identicon is used to uniquely identify all users on the site. The
   # image is generated from the user's id value.
   identiconUrl: Em.computed 'id', ->
     "http://sigil.cupcake.io/#{@get 'id'}"


#----- Fixture Data ------------------------------------------

User.reopenClass
   FIXTURES: [
      {"id":1,"first":"Deborah","last":"Kelly","email":"dkelly0@rediff.com","spend":70.21,"account": "1","activity": ["7","12","15","72","88"]},
      {"id":2,"first":"Kathy","last":"Ruiz","email":"kruiz1@microsoft.com","spend":16.67,"account": "1","activity": ["5","24","40","79","94"]},
      {"id":3,"first":"James","last":"Fowler","email":"jfowler2@ucoz.com","spend":22.89,"isManaged":false,"account": "2","activity": ["4","23"]},
      {"id":4,"first":"Andrew","last":"Spencer","email":"aspencer3@archive.org","spend":90.17,"account": "3","activity": ["6","33","49","81"]},
      {"id":5,"first":"Julie","last":"Sanders","email":"jsanders4@techcrunch.com","spend":82.71,"isManaged":true,"account": "3","activity": ["27","31","47","67","69","93"]},
      {"id":6,"first":"Mary","last":"Robinson","email":"mrobinson5@fotki.com","spend":98.7,"isManaged":false,"account": "3","activity": ["18","21","83","84"]},
      {"id":7,"first":"Jose","last":"Bell","email":"jbell6@1688.com","spend":76.2,"isManaged":false,"account": "4","activity": ["2","34","35","37"]},
      {"id":8,"first":"Stephanie","last":"Perry","email":"sperry7@example.com","spend":72.18,"isManaged":true,"account": "4","activity": ["3","58","65","90"]},
      {"id":9,"first":"William","last":"Spencer","email":"wspencer8@hibu.com","spend":65.88,"isAdmin":true,"account": "5","activity": ["10","22","82","97","100"]},
      {"id":10,"first":"Bonnie","last":"Mcdonald","email":"bmcdonald9@harvard.edu","spend":93.17,"isAdmin":false,"account": "6","activity": ["11","25","38","39","50","55","59","70","76","77"]},
      {"id":11,"first":"Kimberly","last":"Gonzales","email":"kgonzalesa@msu.edu","spend":8.25,"account": "7","activity": ["64","80"]},
      {"id":12,"first":"Aaron","last":"Castillo","email":"acastillob@hatena.ne.jp","spend":12.83,"account": "7","activity": ["63","98"]},
      {"id":13,"first":"Wayne","last":"Hunter","email":"whunterc@dyndns.org","spend":97.73,"account": "7","activity": ["1","9","62","74","78","87"]},
      {"id":14,"first":"Matthew","last":"Stanley","email":"mstanleyd@360.cn","spend":78.06,"account": "7","activity": ["8","48","68","95"]},
      {"id":15,"first":"Donna","last":"Hall","email":"dhalle@google.de","spend":20.33,"isManaged":false,"account": "8","activity": ["14","41","43","51","60","91"]},
      {"id":16,"first":"Denise","last":"Hunt","email":"dhuntf@springer.com","spend":31.25,"account": "8","activity": ["53","86","96","99"]},
      {"id":17,"first":"Alan","last":"Stephens","email":"astephensg@biglobe.ne.jp","spend":1.1,"isManaged":false,"account": "8","activity": ["16","26","52","56","89"]},
      {"id":18,"first":"Bobby","last":"Rivera","email":"briverah@nymag.com","spend":84.92,"isAdmin":true,"isManaged":false,"account": "9","activity": ["13","17","29","30","36","42","44","66","73","75","85"]},
      {"id":19,"first":"Deborah","last":"Hernandez","email":"dhernandezi@cam.ac.uk","spend":54.78,"account": "9","activity": ["20","28","54","57","61"]},
      {"id":20,"first":"Eugene","last":"Hansen","email":"ehansenj@yale.edu","spend":72.93,"account": "10","activity": ["19","32","45","46","71","92"]}
   ]

`export default User`
