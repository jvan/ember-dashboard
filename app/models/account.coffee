`import DS from 'ember-data'`

Account = DS.Model.extend
   
   #----- Attributes --------------------------------------------

   name:    DS.attr 'string'
   street:  DS.attr 'string'
   suite:   DS.attr 'string'
   city:    DS.attr 'string'
   state:   DS.attr 'string'
   zip:     DS.attr 'string'
   phone:   DS.attr 'string'
   attn:    DS.attr 'string'
   budget:  DS.attr 'number'

   #----- Relationships -----------------------------------------

   users: DS.hasMany 'user', async: true

   #----- Computed Properties -----------------------------------

   cityStateZip: Em.computed 'city', 'state', 'zip', ->
      "#{@get 'city'}, #{@get 'state'} #{@get 'zip'}"

   # Return the total amount spent by all users beloging to the account.
   totalSpend: Em.computed 'users.@each.spend', ->
      users = @get 'users'
      users.reduce ((total, user) -> total + user.get('spend')), 0


#----- Fixture Data ------------------------------------------

Account.reopenClass
   FIXTURES: [
      {"id":1,"name":"Skinte","street":"58454 Hallows Plaza","city":"United States","state":"Florida","zip":"33625","attn":"Kathleen Riley","budget":2464.00,"users":["1", "2"]},
      {"id":2,"name":"Avavee","street":"852 Florence Crossing","city":"United States","state":"Delaware","zip":"19897","phone":"3-(670)148-2636","attn":"Carolyn Morrison","budget":11987.00,"users":["3"]},
      {"id":3,"name":"Teklist","street":"7618 Loomis Court","city":"United States","state":"Alaska","zip":"99599","attn":"Bruce Schmidt","budget":14969.00,"users":["4","5","6"]},
      {"id":4,"name":"Wikizz","street":"56 Northland Center","city":"United States","state":"Florida","zip":"32859","attn":"Roy Alvarez","budget":21550.00,"users":["7","8"]},
      {"id":5,"name":"Browseblab","street":"36 Pepper Wood Avenue","city":"United States","state":"California","zip":"90810","phone":"1-(660)213-8418","budget":21815.00,"users":["9"]},
      {"id":6,"name":"Geba","street":"25023 Crescent Oaks Court","city":"United States","state":"California","zip":"92115","phone":"7-(013)231-1551","budget":17312.00,"users":["10"]},
      {"id":7,"name":"Trupe","street":"79 Tennessee Center","city":"United States","state":"North Carolina","zip":"28215","budget":11914.00,"users":["11","12","13","14"]},
      {"id":8,"name":"Photofeed","street":"484 Schmedeman Lane","city":"United States","state":"South Carolina","zip":"29403","attn":"Shawn Payne","budget":6646.00,"users":["15","16","17"]},
      {"id":9,"name":"Bubblebox","street":"54517 Sundown Street","city":"United States","state":"Kansas","zip":"66225","phone":"4-(671)924-1483","budget":17646.00,"users":["18","19"]},
      {"id":10,"name":"Skalith","street":"2 Kings Point","city":"United States","state":"Georgia","zip":"30061","budget":12847.00,"users":["20"]}
   ]

`export default Account`
