`import Ember from 'ember'`

formatCurrency = (value) ->
  "$#{value.toFixed(2)}"

FormatCurrencyHelper = Ember.Handlebars.makeBoundHelper formatCurrency

`export { formatCurrency }`

`export default FormatCurrencyHelper`
