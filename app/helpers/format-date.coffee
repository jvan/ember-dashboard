`import Ember from 'ember'`

formatDate = (value) ->
  moment(value).format('MMM DD YYYY')

FormatDateHelper = Ember.Handlebars.makeBoundHelper formatDate

`export { formatDate }`

`export default FormatDateHelper`
