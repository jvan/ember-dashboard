`import Ember from 'ember'`

formatTime = (value) ->
  moment(value).format('hh:mm A')

FormatTimeHelper = Ember.Handlebars.makeBoundHelper formatTime

`export { formatTime }`

`export default FormatTimeHelper`
