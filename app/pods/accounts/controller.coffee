`import Ember from 'ember'`
`import ItemFilterMixin from 'dashboard/mixins/item-filter'`

AccountsController = Ember.ArrayController.extend ItemFilterMixin,
   foo: "testing"

   filteredModel: Em.computed 'searchFilter', '@each.name', ->
      @filterItems 'name'

   count: Em.computed '@each.isNew', ->
      @get('model').filterBy('isNew', false).get 'length'

   totalSpend: Em.computed '@each.totalSpend', ->
      @get('model').reduce ((total, account) -> total + account.get('totalSpend')), 0


`export default AccountsController`
