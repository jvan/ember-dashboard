`import Ember from 'ember'`

AccountRoute = Ember.Route.extend
   model: (params) ->
      @store.find 'account', params.account_id

`export default AccountRoute`
