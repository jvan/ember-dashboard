`import Ember from 'ember'`
`import AccountValidator from 'dashboard/validators/account'`
`import ErrorValidator from 'dashboard/mixins/error-validator'`

AccountNewController = Ember.Controller.extend ErrorValidator,
   actions:
      save: ->
         account = @get 'model'

         if @validate(AccountValidator, account)
            account.save().then =>
               @set 'model', null
               @transitionToRoute 'accounts'

      cancel: ->
         account = @get 'model'

         if account.get 'isNew'
            @set 'model', null
            account.destroyRecord()

         @transitionToRoute 'accounts'


`export default AccountNewController`
