`import Ember from 'ember'`

AccountsNewRoute = Ember.Route.extend
   model: ->
      if @get('controller')?.get('content')
         return @get('controller').get('content')

      @store.createRecord 'account'
   
`export default AccountsNewRoute`
