`import Ember from 'ember'`

LogErrorsRoute = Ember.Route.extend
   model: ->
      @store.find('message').then (messages) ->
         messages.filterBy('logLevel', 'error')


`export default LogErrorsRoute`
