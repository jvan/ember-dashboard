`import Ember from 'ember'`

LogActivityController = Ember.ArrayController.extend
  sortProperties: [ 'date:desc' ]
  sortedActivity: Em.computed.sort 'model', 'sortProperties'

`export default LogActivityController`
