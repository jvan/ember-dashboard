`import Ember from 'ember'`

UtilitiesPlatformAlertController = Ember.Controller.extend
   needs: ['application']
   application: Em.computed.alias 'controllers.application'

   message: null

   actions:
      broadcast: ->
         @get('application').send 'broadcast', @get('message')

`export default UtilitiesPlatformAlertController`
