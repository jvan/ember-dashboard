`import Ember from 'ember'`
`import UserValidator from 'dashboard/validators/user'`
`import ErrorValidatorMixin from 'dashboard/mixins/error-validator'`

UserNewController = Ember.Controller.extend ErrorValidatorMixin,
   needs: ['accounts']
   accounts: Em.computed.alias 'controllers.accounts'

   actions:
      save: ->
         user = @get 'model'

         if @validate(UserValidator, user)
            user.save().then =>
               @set 'model', null
               @transitionToRoute 'users'

      cancel: ->
         user = @get 'model'

         if user.get 'isNew'
            @set 'model', null
            user.destroyRecord()

         @transitionToRoute 'users.all'

`export default UserNewController`
