`import Ember from 'ember'`

UserNewBulkUploadController = Ember.Controller.extend
  needs: ['accounts']
  accounts: Em.computed.alias 'controllers.accounts'

  file:  null
  data:  null
  users: null
  separator: ','

  getFileContents: Em.observer 'file', ->
    if not @get('file')? then return
    console.log '[getting file contents]'

    # Create a file reader and set the `onload` function which will be called once
    # the file contents have been read.
    reader = new FileReader()
    reader.onload = (e) =>
      # Store the file contents and enable the accept button on the modal.
      @set 'data', reader.result

    reader.readAsText(@file)

  processFileData: Em.observer 'data', 'separator', ->
    if not @get('data')? then return
    console.log '[processing user data]'

    @set 'users', Em.A()

    console.log @get('data')

    lines = @get('data').split('\n')
    lines = lines.filter (line) -> line != ''

    lines.forEach (line) =>

      elems = line.split @get('separator')

      first =   elems[0]
      last =    elems[1]
      email =   elems[2]
      account = elems[3]

      @store.find('account').then (accounts) =>
        account = accounts.filterBy 'name', account
        
        user = @store.createRecord 'user',
            first: first
            last: last
            email: email
            account: account[0]

        @get('users').pushObject user

  userCount: Em.computed 'users.@each', ->
    @get('users.length')

  actions:
    uploadFile: ->
      Em.$('#upload-input').click()

    removeUser: (user) ->
      @get('users').removeObject user

    save: ->
      console.log '[saving users]'

      promises = []

      @get('users').forEach (user) ->
        promise = user.save()
        promises.push promise

      Em.RSVP.all(promises).then =>
        console.log '  -- all users saved'

        @set 'users', null
        @set 'file',  null
        @set 'data',  null

        @transitionToRoute 'users.all'

    cancel: ->
        @set 'users', null
        @set 'file',  null
        @set 'data',  null

        @transitionToRoute 'users.all'


`export default UserNewBulkUploadController`
