`import Ember from 'ember'`

UsersNewRoute = Ember.Route.extend
   model: ->
      if @get('controller')?.get('content')
         return @get('controller').get('content')

      @store.createRecord 'user'

`export default UsersNewRoute`
