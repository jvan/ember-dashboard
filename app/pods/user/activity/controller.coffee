`import Ember from 'ember'`

UserActivityController = Ember.ObjectController.extend
  sortProperties: [ 'date:desc' ]
  sortedActivity: Em.computed.sort 'activity', 'sortProperties'

`export default UserActivityController`
