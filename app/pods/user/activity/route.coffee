`import Ember from 'ember'`

UserActivityRoute = Ember.Route.extend
  model: ->
    @modelFor 'user'

`export default UserActivityRoute`
