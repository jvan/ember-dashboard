`import Ember from 'ember'`

UserProfileRoute = Ember.Route.extend
   model: ->
      @modelFor 'user'

   setupController: (controller, model) ->
     @_super arguments...

     @store.find('account').then (accounts) ->
       controller.set 'accounts', accounts


`export default UserProfileRoute`
