`import Ember from 'ember'`

UserEditController = Ember.Controller.extend
   needs: ['accounts']
   accounts: Em.computed.alias 'controllers.accounts'

`export default UserEditController`
