`import Ember from 'ember'`

UsersAdminRoute = Ember.Route.extend
   templateName: 'users'
   controllerName: 'users'

   model: ->
      @store.filter 'user', (user) ->
         user.get 'isAdmin'

`export default UsersAdminRoute`
