`import Ember from 'ember'`

UsersManagedRoute = Ember.Route.extend
   templateName: 'users'
   controllerName: 'users'

   model: ->
      @store.filter 'user', (user) ->
         user.get 'isManaged'

`export default UsersManagedRoute`
