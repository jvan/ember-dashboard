`import Ember from 'ember'`

UsersAllRoute = Ember.Route.extend
   templateName: 'users'
   controllerName: 'users'

   model: ->
      @store.find 'user'

`export default UsersAllRoute`
