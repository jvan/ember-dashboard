`import Ember from 'ember'`
`import ItemFilterMixin from 'dashboard/mixins/item-filter'`

UsersController = Ember.ArrayController.extend ItemFilterMixin,
   allUsers: null

   filteredModel: Em.computed 'searchFilter', '@each.fullname', ->
      @filterItems 'fullname'

   count: Em.computed 'allUsers.@each.isNew', ->
      @get('allUsers')?.filterBy('isNew', false).get 'length'

   adminUserCount: Em.computed 'allUsers.@each.isAdmin', ->
      @get('allUsers')?.filterBy('isAdmin').get 'length'

   managedUserCount: Em.computed 'allUsers.@each.isManaged', ->
      @get('allUsers')?.filterBy('isManaged').get 'length'


`export default UsersController`
