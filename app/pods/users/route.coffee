`import Ember from 'ember'`

UsersRoute = Ember.Route.extend
   model: ->
      @store.find 'user'

   setupController: (controller, model) ->
      @store.find('user').then (users) ->
         controller.set 'allUsers', users

`export default UsersRoute`
