`import Ember from 'ember'`

ErrorValidatorMixin = Ember.Mixin.create
   errors: { }

   validate: (validatorClass, model) ->
      validator = new validatorClass(model)
      validator.validate()

      @send 'clearErrors'

      if not validator.isValid()
         @set 'errors', validator.errors
         return false

      true

   actions:
      clearErrors: ->
         @set 'errors', { }

`export default ErrorValidatorMixin`
