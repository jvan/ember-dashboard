`import Ember from 'ember'`

ItemSelectMixin = Ember.Mixin.create

   selectedItems: Em.computed 'content.@each.isSelected', ->
      items = @get 'content'
      items.filter (item) ->
         item.get 'isSelected'

   selectedCount: Em.computed 'content.@each.isSelected', ->
      @get 'selectedItems.length'

   allSelected: Em.computed 'content.@each.isSelected', (key, value) ->
      items = @get 'content'
      if not value?
         return items.isEvery 'isSelected', true
      else
         items.setEach 'isSelected', value


`export default ItemSelectMixin`
