`import Ember from 'ember'`

ItemFilterMixin = Ember.Mixin.create
   searchFilter: ''

   filterItems: (field) ->
      filter = @get 'searchFilter'
      rx = new RegExp filter, 'gi'
      items = @get('model')
      items.filter (item) ->
         item.get(field)?.match(rx)

   actions:
      clearSearchFilter: ->
         @set 'searchFilter', ''


`export default ItemFilterMixin`
