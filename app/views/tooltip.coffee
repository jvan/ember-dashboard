`import Ember from 'ember'`

TooltipView = Ember.View.extend
   tagName:  'span'
   title:  null
   html: true
   placement:  'top'
   show: false
   didInsertElement: (->
      @s = @$()
      @s.tooltip('destroy')
      @s.tooltip
         html:      @get 'html'
         title:     @get 'title'
         placement: @get 'placement'
         container: 'body'
      if @show
        @s.tooltip('show')
   ).observes('title')
   willDestroy: ->
      @s.tooltip('destroy')

`export default TooltipView`
