`import Ember from 'ember'`

ApplicationView = Ember.View.extend

   # Initialize javascript components once the element has been
   # inserted into the DOM.
   didInsertElement: ->

      # Add a click action handler to the side navigation toggles.
      Em.$('#side-nav .toggle').click ->

         # Slide the sub-menu in/out.
         $(this).next().slideToggle()

         # Toggle the expand icon to reflect the current state of
         # the sub-menu.
         icon = $(this).find('span > i')

         # Check if the sub-menu was expanded (before toggling)
         # and update the expand icon.
         if icon.hasClass('fa-caret-down')
            icon.removeClass('fa-caret-down')
                .addClass('fa-caret-right')
         else
            icon.removeClass('fa-caret-right')
                .addClass('fa-caret-down')
         

      # Enable the affix plugin on the side nav.
      Em.$('#side-nav').affix
         # Set the top offset equal to the height of the top nav bar.
         offset:
            top: Em.$('#top-nav').height()


`export default ApplicationView`
