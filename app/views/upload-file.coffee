`import Ember from 'ember'`

UploadFileView = Ember.TextField.extend
   type: "file"

   change: (event) ->
      input = event.target
      filename = input.files[0].name
      console.log "  --  filename=#{filename}"

      @ctrl.set 'file', input.files[0]

`export default UploadFileView`
