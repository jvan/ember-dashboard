`import Ember from 'ember'`

LineChartComponent = Ember.Component.extend
   tagName: 'svg'
   attributeBindings: 'width height data'.w()

   margin: { top: 20, right: 20, bottom: 30, left: 40 }

   w: Em.computed 'width', ->
      @get('width') - @get('margin.left') - @get('margin.right')

   h: Em.computed 'height', ->
      @get('height') - @get('margin.top') - @get('margin.bottom')

   transformG: Em.computed '', ->
      "translate(#{@get('margin.left')}, #{@get('margin.top')})"

   transformX: Em.computed 'h', ->
      "translate(0, #{@get('h')})"

   didInsertElement: ->
      elementId = @get('elementId')

      data = @get 'data'

      width  = @get 'w'
      height = @get 'h'

      #parseDate = d3.time.format('%Y%m%d').parse

      @svg = d3.select('#'+elementId)
               .append("g")
               .attr('transform', @get('transformG'))

      @scale = {
         x: d3.scale.linear()
              .range([0, width]),

         y: d3.scale.linear()
              .range([height, 0])
      }

      #@scale.x.domain(d3.extent(data, (d) -> parseDate(d.date)))

      @scale.x.domain([0, data.get('length')])
      @scale.y.domain(d3.extent(data, (d) -> d.value))

      @line = d3.svg.line()
                .interpolate('basis')
                .x((d,i) => @scale.x(i))
                .y((d)   => @scale.y(d.value))

      @axis = {
         x: d3.svg.axis()
              .scale(@scale.x)
              .orient('bottom'),

         y: d3.svg.axis()
              .scale(@scale.y)
              .orient('left')
      }

      @draw()
      @drawAxes()

   drawLines: ->
      data = @get 'data'

      lines = @svg.selectAll('.line')
                  .data([data])

      lines.enter()
           .append('path')
           .attr('class', 'line')
      
      lines.attr('d', (d) => @line(d))


      lines.exit().remove()

   drawAxes: ->
      @svg.append('g')
          .attr('class', 'x axis')
          .attr('transform', "translate(0, #{@get 'h'})")
          .call(@axis.x)

      @svg.append('g')
          .attr('class', 'y axis')
          .call(@axis.y)
          .append('text')
          .attr('transform', 'rotate(-90)')
          .attr('y', 6)
          .attr('dy', '.71em')
          .style('text-anchor', 'end')

   draw: Em.observer 'data.@each.[]', ->
      @drawLines()

`export default LineChartComponent`
