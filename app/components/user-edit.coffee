`import Ember from 'ember'`

randomPassword = (length) ->
   charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()'
   password = [1 .. length].map (i) -> charset[Math.floor(charset.length*Math.random())]
   password.join('')

UserEditComponent = Ember.Component.extend
   isEditing: false

   actions:
      toggleEditing: ->
         @toggleProperty 'isEditing'

      save: ->
         @get('user').save()
         @toggleProperty 'isEditing'

      cancel: ->
         @get('user').rollback()
         @toggleProperty 'isEditing'

      generatePassword: ->
         @set 'user.password', randomPassword(13)


`export default UserEditComponent`
