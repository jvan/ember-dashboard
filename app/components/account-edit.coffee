`import Ember from 'ember'`

AccountEditComponent = Ember.Component.extend
   isEditing: false

   actions:
      toggleEditing: ->
         @toggleProperty 'isEditing'

       save: ->
          @get('account').save()
          @toggleProperty 'isEditing'

      cancel: ->
         @get('account').rollback()
         @toggleProperty 'isEditing'


`export default AccountEditComponent`
