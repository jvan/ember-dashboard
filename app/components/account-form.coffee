`import Ember from 'ember'`

AccountFormComponent = Ember.Component.extend
   actions:
      save: ->
         console.log '[account form]: save'
         @sendAction 'onSave'

      cancel: ->
         console.log '[account form]: cancel'
         @sendAction 'onCancel'
         

`export default AccountFormComponent`
