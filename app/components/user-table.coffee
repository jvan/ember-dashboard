`import Ember from 'ember'`
`import ItemSelectMixin from 'dashboard/mixins/item-select'`

UserTableComponent = Ember.Component.extend ItemSelectMixin,
   actions:
      deleteUsers: ->
         console.log '[user table component]: delete selected'

         users = @get 'selectedItems'

         users.forEach (user) ->

            user.get('account').then (account) ->
               account.get('users').removeObject user
               account.save()

            user.destroyRecord()

`export default UserTableComponent`
