`import Ember from 'ember'`
`import ItemSelectMixin from 'dashboard/mixins/item-select'`

AccountTableComponent = Ember.Component.extend ItemSelectMixin,
   actions:
      deleteAccounts: ->
         accounts = @get 'selectedItems'

         accounts.forEach (account) ->
            account.destroyRecord()

`export default AccountTableComponent`
