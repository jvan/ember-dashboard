`import Validator from './validator'`

class AccountValidator extends Validator
  validators:
    name: (name) ->
      if not name
        return 'You must enter an account name'

      null

`export default AccountValidator`
