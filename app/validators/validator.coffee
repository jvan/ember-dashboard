class Validator
  validators: { }
  constructor: (@args) ->

  addValidator: (field, func) ->
    @validators[field] = func

  validate: ->
    @errors = { }
    for field, func of @validators
      @errors[field] = func @args.get(field)

  isValid: ->
    failed = (true for field, err of @errors when err != null)
    failed.length == 0

`export default Validator`
