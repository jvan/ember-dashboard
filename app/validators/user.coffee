`import Validator from './validator'`

class UserValidator extends Validator
  validators:
    first: (name) ->
      if not name
        return 'You must enter a first name'
      null

    last: (name) ->
      if not name
        return 'You must enter a last name'
      null

    email: (email) ->
      if not email
        return 'You must enter an email'
      null


`export default UserValidator`
